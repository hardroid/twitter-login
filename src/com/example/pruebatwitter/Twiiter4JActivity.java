package com.example.pruebatwitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Twiiter4JActivity extends ActionBarActivity {

	//TWitter
	private static Twitter twitter;
	private static RequestToken requestToken;
	private AccessToken accessToken;

	// Shared Preferences
	private static SharedPreferences mSharedPreferences;

	static String TWITTER_CONSUMER_KEY = "mZP7eU8EccwU7f5kckvq7iwe5"; // place your cosumer key here
	static String TWITTER_CONSUMER_SECRET = "LTHRp7WjryL44CLqgKb7ihlXL7a570hpQYl1GZeE3BueQvhV6M"; // place your consumer secret here

	// Preference Constants
	static String PREFERENCE_NAME = "twitter_oauth";
	static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";

	static final String TWITTER_CALLBACK_URL = "oauth://t4jsample";

	// Twitter oauth urls
	static final String URL_TWITTER_AUTH = "auth_url";
	static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twiiter4_j);

		// Shared Preferences
		mSharedPreferences = getApplicationContext().getSharedPreferences(
				"MyPref", 0);

		/** This if conditions is tested once is
		 * redirected from twitter page. Parse the uri to get oAuth
		 * Verifier
		 * */
		if (!isTwitterLoggedInAlready()) {
			Uri uri = getIntent().getData();
			if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL)) {
				// oAuth verifier
				final String verifier = uri
						.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
				if(verifier != null){
					new ObtenerDatosTwitter().execute(verifier);
				}

			}
		}

	}

	public void onClickLogin(View v){
		loginToTwitter();
	}

	public void onClickLogout(View v){
		logoutFromTwitter();
	}



	/**
	 * Function to login twitter
	 * */
	private void loginToTwitter() {
		// Check if already logged in
		if (!isTwitterLoggedInAlready()) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();


			Thread thread = new Thread(new Runnable(){
				@Override
				public void run() {
					try {

						requestToken = twitter
								.getOAuthRequestToken(TWITTER_CALLBACK_URL);
						Twiiter4JActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
								.parse(requestToken.getAuthenticationURL())));

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			thread.start();         
		} else {
			// user already logged into twitter
			Toast.makeText(getApplicationContext(),
					"Already Logged into twitter", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Function to logout from twitter
	 * It will just clear the application shared preferences
	 * */
	private void logoutFromTwitter() {
		// Clear the shared preferences
		Editor e = mSharedPreferences.edit();
		e.remove(PREF_KEY_OAUTH_TOKEN);
		e.remove(PREF_KEY_OAUTH_SECRET);
		e.remove(PREF_KEY_TWITTER_LOGIN);
		e.commit();      
	}

	/**
	 * Check user already logged in your application using twitter Login flag is
	 * fetched from Shared Preferences
	 * */
	private boolean isTwitterLoggedInAlready() {
		// return twitter login status from Shared Preferences
		return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	}

	class ObtenerDatosTwitter extends AsyncTask<String, Void, AccessToken>{

		@Override
		protected AccessToken doInBackground(String... params) {
			try {
				String verifier = params[0];
				AccessToken retorno = twitter.getOAuthAccessToken(requestToken, verifier); 
				return retorno;
			} catch (TwitterException e1) {
				return null;
			}        	
		}

		@Override
		protected void onPostExecute(AccessToken result) {
			if(result != null){
				// Shared Preferences
				Editor e = mSharedPreferences.edit();

				// After getting access token, access token secret
				// store them in application preferences
				e.putString(PREF_KEY_OAUTH_TOKEN, result.getToken());
				e.putString(PREF_KEY_OAUTH_SECRET,
						result.getTokenSecret());
				// Store login status - true
				e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
				e.commit(); // save changes

				Log.e("Twitter OAuth Token", "> " + result.getToken());

				// Getting user details from twitter
				// For now i am getting his name only
				long userID = result.getUserId();
				String user;
				user = result.getScreenName();

				Toast.makeText(getApplicationContext(), "id: " + userID + ", nombre: " + user, Toast.LENGTH_LONG).show();
			}
		}



	}

}
