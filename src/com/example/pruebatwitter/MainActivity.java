package com.example.pruebatwitter;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


public class MainActivity extends ActionBarActivity {


	private static CommonsHttpOAuthProvider provider = new CommonsHttpOAuthProvider(
			"https://api.twitter.com/oauth/request_token",
			"https://api.twitter.com/oauth/access_token",
			"https://api.twitter.com/oauth/authorize");
	private static CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer(
			"mZP7eU8EccwU7f5kckvq7iwe5", 
			"LTHRp7WjryL44CLqgKb7ihlXL7a570hpQYl1GZeE3BueQvhV6M");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public void onResume(){
		super.onResume();
		Log.e("Twitter", "OnResume" + consumer.getToken());
		
	}

	public void onClickLogin(View v){
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				loginTwitter();
			}
		});
		
		thread.start();
       
	}
	
	private void loginTwitter(){
		String authUrl;
		try {
			authUrl = provider.retrieveRequestToken(consumer, "mdw://twitter");
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)));
		} catch (OAuthMessageSignerException | OAuthNotAuthorizedException
				| OAuthExpectationFailedException | OAuthCommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClickLogOut(View v){
		
	}
}
